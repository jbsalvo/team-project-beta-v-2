from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.name
    
    def get_api_url(self):
        return reverse("api_show_sales_person", kwargs={"pk": self.pk})


class Address(models.Model):
    street = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=2)
    zipcode = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.street}, {self.city}, {self.state} {self.zipcode}'


class PotentialCustomer(models.Model):
    name = models.CharField(max_length=100)
    address = models.ForeignKey('Address', related_name="customers", on_delete=models.CASCADE)
    phone = models.CharField(max_length=15)

    def __str__(self):
        return self.name

class SalesRecord(models.Model):
    automobile = models.OneToOneField(
        AutomobileVO,
        related_name="sales_records",
        on_delete=models.PROTECT,
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales_records",
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="sales_records",
        on_delete=models.PROTECT,
    )
    price = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.sales_person} sold {self.automobile} to {self.customer} for ${self.price}'