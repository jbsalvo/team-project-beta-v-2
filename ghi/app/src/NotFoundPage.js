import React from 'react';

class NotFoundPage extends React.Component{
    render(){
        return (
            <div className='justify-content-evenly'>
                <div className='col-12 p-3'>
                    <img className='img-fluid' src='https://images.pexels.com/photos/6140456/pexels-photo-6140456.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2' alt='lost_img' />
                    <h1 className='carousel-caption text-center'>The page you are looking for does not exist..</h1>
                </div>
            </div>
        );
    }
}
export default NotFoundPage;
