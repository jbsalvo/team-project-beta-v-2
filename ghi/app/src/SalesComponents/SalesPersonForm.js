import React from "react";

class SalesPersonForm extends React.Component {
    constructor(){
        super()
        this.state = {
            name: "",
            employee_number: "",

        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state};
        const employeeUrl = "http://localhost:8090/api/sales/salespeople/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(employeeUrl, fetchConfig);
        if (response.ok) {
            const newEmployee = await response.json();
            const cleared = {
                name: "",
                employee_number: "",
            };
            this.setState(cleared);
            const successAlert = document.getElementById("success-message")
            successAlert.classList.remove("d-none")

        };
    }

    handleInputChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({...this.state, [name]: value})
    }

    render() {
        return (
            <div className='container pt-5'>
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add Sales Person</h1>
                        <form onSubmit={this.handleSubmit} id="create-sales-person-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                                <label htmlFor="Name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleInputChange} value={this.state.employee_number} placeholder="Employee Number" required type="number" name="employee_number" id="name" className="form-control"/>
                                <label htmlFor="employee_number">Enter Employee Number</label>
                            </div>
                            <button className="btn btn-success">Add</button>
                        </form>
                        <div className="alert alert-success d-none mt-5" id="success-message">
                            Successfully added a new sales person!
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default SalesPersonForm;
