import React from 'react';
import { Link } from 'react-router-dom';

class SalesLanding extends React.Component {
    render () {
        return (
            <div className='container pt-5'>
                <div className="row justify-content-evenly m-5">
                        <div className="col-6 col-sm-2 shadow p-1 m-1 text-success bg-white rounded">
                            <h3 className="text-center">Add A Sale</h3>
                            <Link to="/sales/new-sale">
                                <img src="/sold.png" className="img-fluid" alt="sold img"/>
                            </Link>
                        </div>
                        <div className="col-6 col-sm-2 shadow p-1 m-1 text-success bg-white rounded">
                            <h3 className="text-center">Sales History</h3>
                            <Link to="/sales/sales-history">
                                <img src="/history.png" className="img-fluid" alt="hist img" />
                            </Link>
                        </div>
                        <div className="col-6 col-sm-2 shadow p-1 m-1 text-success bg-white rounded">
                            <h3 className="text-center">Individual Sales</h3>
                            <Link to="/sales/search-by-employee">
                                <img src="/ind_sales.png" className="img-fluid" alt="sales person" />
                            </Link>
                        </div>
                </div>
                <div className="row justify-content-evenly m-5">
                    <div className="col-6 col-sm-2 shadow p-1 m-1 text-success bg-white rounded">
                        <h3 className="text-center">Add Salesman</h3>
                        <Link to="/sales/new-employee">
                            <img src="/cust.png" className="img-fluid" alt="add salesperson" />
                        </Link>
                    </div>
                    <div className="col-6 col-sm-2 shadow p-1 m-1 text-success bg-white rounded">
                        <h3 className="text-center">Add Customer</h3>
                        <Link to="/sales/new-customer">
                            <img src="/cust.png" className="img-fluid" alt="add customer" />
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default SalesLanding;
