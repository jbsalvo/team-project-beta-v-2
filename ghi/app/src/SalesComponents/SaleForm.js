import React from 'react';

class SaleForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            automobile: '',
            automobiles: [],
            sales_person: '',
            sales_people: [],
            customer: '',
            customers: [],
            price: ''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleInputChange(event) {
        this.setState({
          ...this.state,
          [event.target.name]: event.target.value
        })
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.automobiles;
        delete data.sales_people;
        delete data.customers;

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {

          const cleared = {
            automobile: '',
            price: '',
            sales_person: '',
            customer: '',
          };
          this.setState(cleared);
          // this.componentDidMount();
          const successAlert = document.getElementById("success-message")
          successAlert.classList.remove("d-none")
        }
      }

    async componentDidMount(){
        const inventory_url = "http://localhost:8090/api/sales/available-autos/";
        const sales_url = 'http://localhost:8090/api/sales/salespeople/';
        const cust_url = 'http://localhost:8090/api/sales/customers/';
        const response1 = await fetch(inventory_url);
        const response2 = await fetch(sales_url);
        const response3 = await fetch(cust_url);
        if (response1.ok && response2.ok && response3.ok){
            const autoData  = await response1.json();
            const salesData = await response2.json();
            const customerData = await response3.json();
            this.setState({automobiles:autoData.available_autos, sales_people:salesData.sales_people, customers:customerData.customers});
        }
    }

    render() {
      return (
        <div className='container pt-5'>
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a Sale</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="mb-3">
                  <select onChange= {this.handleInputChange} value={this.state.customer} required name="customer" id="customer" className="form-select">
                    <option value="default">Choose a Customer</option>
                    {this.state.customers.map(cust => {
                        return (
                        <option key= {cust.id} value={cust.id}>
                            {cust.name}
                        </option>
                        );
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select onChange= {this.handleInputChange} value={this.state.sales_person} required name="sales_person" id="salesman" className="form-select">
                    <option value="default">Choose a Salesman</option>
                    {this.state.sales_people.map(person => {
                        return (
                        <option key= {person.id} value={person.employee_number}>
                            {person.name}
                        </option>
                        );
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select onChange= {this.handleInputChange} value={this.state.automobile} required name="automobile" id="automobile" className="form-select">
                    <option value="default">Choose a Vehicle</option>
                    {this.state.automobiles.map(car => {
                        return (
                        <option key= {car.vin} value={car.vin}>
                            {car.vin}
                        </option>
                        );
                    })}
                  </select>
                </div>
                <div className="form-floating mb-3">
                  <input onChange= {this.handleInputChange} value={this.state.price} placeholder="Price" required type="text" name="price" id="Price" className="form-control"/>
                  <label htmlFor="style">Purchase Price</label>
                </div>
                <button className="btn btn-success">Create</button>
              </form>
              <div className="alert alert-success d-none mt-5" id="success-message">
                Successfully added a new sale!
              </div>
            </div>
          </div>
        </div>
        </div>
      );
    }
  }

export default SaleForm;
