import { NavLink } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Dropdown from 'react-bootstrap/Dropdown';
{/* ^^^^^^^^^^  https://react-bootstrap.github.io/components/dropdowns/ */}

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-black">
        <div className="container-fluid">
          <NavLink className="navbar-brand" to="/">CarCar</NavLink>

          <div id='sales menu'>
            <Dropdown as={ButtonGroup}>
              <Button variant="dark" href="/sales">Sales</Button>
              <Dropdown.Toggle split variant="dark" id="dropdown-split-basic" />
              <Dropdown.Menu>
                <Dropdown.Item href="/sales/sales-history/">Sales History</Dropdown.Item>
                <Dropdown.Item href="/sales/search-by-employee/">Individual Performance</Dropdown.Item>
                <Dropdown.Item href="/sales/new-employee/">Add New Employee</Dropdown.Item>
                <Dropdown.Item href="/sales/new-customer/">Add New Customer</Dropdown.Item>
                <Dropdown.Item href="/sales/new-sale/">Create a New Sale</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>

          <div className="p-1" id='service menu'>
            <Dropdown as={ButtonGroup}>
              <Button variant="dark" href="/services">Service</Button>
              <Dropdown.Toggle variant="dark" id="dropdown-split-basic" />
              <Dropdown.Menu>
                <Dropdown.Item href="/appointments/">Service Appointments</Dropdown.Item>
                <Dropdown.Item href="/appointments/history/">Search Service History</Dropdown.Item>
                <Dropdown.Item href="/appointments/new/">Schedule an Appointment</Dropdown.Item>
                <Dropdown.Item href="/technicians/">Add New Technician</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>

          <div id='inventory menu'>
          <Dropdown as={ButtonGroup}>
            <Button variant="dark" href="/automobiles">Inventory</Button>
            <Dropdown.Toggle variant="dark" id="dropdown-split-basic" />
            <Dropdown.Menu>
              <Dropdown.Item href="/automobiles/">Automobile Inventory</Dropdown.Item>
              <Dropdown.Item href="/automobiles/new">Add an Automobile</Dropdown.Item>
              <Dropdown.Item href="/manufacturers">Manufacturers</Dropdown.Item>
              <Dropdown.Item href="/manufacturers/new">Add a Manufacturer</Dropdown.Item>
              <Dropdown.Item href="/vehicle-models">Vehicle Models</Dropdown.Item>
              <Dropdown.Item href="/vehicle-models/new">Add a Vehicle Model</Dropdown.Item>
              <Dropdown.Item href="/automobiles/carousel">Tesla Model 3</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          </div>

        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
