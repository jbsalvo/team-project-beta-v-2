import React, { useState, useEffect } from "react";


function ServiceHistory() {
    const [filteredAppointments, setFilteredAppointments] = useState([]);
    const [appointments, setAppointments] = useState([]);


    useEffect(() => {
        const appointmentData = async () => {
            const response = await fetch("http://localhost:8080/api/appointments/");
            const data = await response.json();
            setAppointments(data.appointments.filter(appointment => appointment.finished === true));
            setFilteredAppointments(data.appointments.filter(appointment => appointment.finished === true));
        }
        appointmentData()
    },[])

    const handleInputChange = (event) => {
        let search = event.target.value;
        // console.log("This is the input VIN: ", searchVIN);

        //SOLUTION #1:
        // array with matches
        let appointmentMatches = [];
        // check if searchVIN is a substring in each appointment vin, using forEach
        appointments.forEach(appointment => {
            // if so push that appointment into array
            if (appointment.vin.includes(search) || appointment.customer_name.includes(search) || appointment.technician.name.includes(search)) {
                appointmentMatches.push(appointment);
            }
        });
        console.log(appointmentMatches);

        // SOLUTION #2:
        // let appointmentMatches = appointments.map(appointment => {
        //     if (appointment.vin.includes(searchVIN)) {
        //         return appointment
        //     }
        // }).filter(appointment => appointment)
        // console.log(appointmentMatches)
        // setstate with new appointments array
        setFilteredAppointments(appointmentMatches);
    }


    if (filteredAppointments.length === 0) {
        return(
            <div>
                <form onSubmit={event => {event.preventDefault()}} id="search-vin-form">
                    <div className="input-group mb-3 p-4">
                        <input onChange={handleInputChange} className="form-control" name="searchVIN" id="searchVIN" type="search" placeholder="Search"/>
                    </div>
                </form>
                <h1>Service Appointments History</h1>
                <p>No appointments were found for the VIN you searched</p>
            </div>
        )
        } else {
            return (
                <div>
                    <form onSubmit={event => {event.preventDefault()}} id="search-vin-form">
                        <div className="input-group mb-3 p-4">
                            <input onChange={handleInputChange} className="form-control" name="searchVIN" id="searchVIN" type="search" placeholder="Search"/>
                        </div>
                    </form>
                    <h1>Service Appointments History</h1>
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>VIN</th>
                                    <th>Customer Name</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Reason</th>
                                    <th>Technician</th>
                                    <th>VIP</th>
                                </tr>
                            </thead>
                            <tbody>
                                {filteredAppointments.map(appointment => {
                                    return (
                                        <tr key={ appointment.id }>
                                            <td>{ appointment.vin }</td>
                                            <td>{ appointment.customer_name }</td>
                                            <td>{ appointment.date }</td>
                                            <td>{ appointment.time }</td>
                                            <td>{ appointment.reason }</td>
                                            <td>{ appointment.technician.name }</td>
                                            { appointment.vip && <td>Yes</td> }
                                            { !appointment.vip && <td>No</td> }
                                        </tr>
                                    );
                                })}
                            </tbody>
                        </table>
                </div>
            )
    }
}


export default ServiceHistory






// import React from "react";


// class ServiceHistory extends React.Component {
//     constructor() {
//         super();
//         this.state = {
//             searchVIN: "",
//             appointments: [],
//             backToFilteredAppointments: [],
//          };
//     }


//     handleSubmit = async (event) => {
//         event.preventDefault();
//         const data = {...this.state};
//         const filteredVIN = data.appointments.filter(appointment => appointment.vin === data.searchVIN)
//             if (data.searchVIN) {
//                 this.setState({ appointments: filteredVIN })
//             }
//             else {
//                 this.setState({ appointments: this.state.backToFilteredAppointments });
//             }
//     }


//     handleInputChange = (event) => {
//         const name = event.target.name;
//         const value = event.target.value;
//         this.setState({...this.state, [name]: value})
//     }


//     async componentDidMount() {
//         const response = await fetch ("http://localhost:8080/api/appointments/");
//         if (response.ok) {
//             const data = await response.json();
//             const filteredAppointments = data.appointments.filter(appointment => appointment.finished === true);
//             this.setState({ appointments: filteredAppointments,
//                 backToFilteredAppointments: filteredAppointments});
//         }
//         else {
//             console.error(response)
//         }
//     }


//     render () {
//         return (
//             <div>
//                 <form onSubmit={this.handleSubmit} id="search-vin-form">
//                     <div className="input-group mb-3 p-4">
//                         <input onChange={this.handleInputChange} value={this.state.searchVIN} className="form-control" name="searchVIN" id="searchVIN" type="search" placeholder="Search VIN"/>
//                         <div className="px-3">
//                             <button className="btn btn-outline-success" type="submit">Search VIN</button>
//                         </div>
//                     </div>
//                 </form>
//                 <h1>Service appointments history</h1>
//                 <table className="table table-striped">
//                     <thead>
//                         <tr>
//                             <th>VIN</th>
//                             <th>Customer Name</th>
//                             <th>Date</th>
//                             <th>Time</th>
//                             <th>Reason</th>
//                             <th>Technician</th>
//                             <th>VIP</th>
//                         </tr>
//                     </thead>
//                     <tbody>
//                         {this.state.appointments.map(appointment => {
//                             return (
//                                 <tr key={ appointment.id }>
//                                     <td>{ appointment.vin }</td>
//                                     <td>{ appointment.customer_name }</td>
//                                     <td>{ appointment.date }</td>
//                                     <td>{ appointment.time }</td>
//                                     <td>{ appointment.reason }</td>
//                                     <td>{ appointment.technician.name }</td>
//                                     { appointment.vip && <td>Yes</td> }
//                                     { !appointment.vip && <td>No</td> }

//                                 </tr>
//                             );
//                         })}
//                     </tbody>
//                 </table>
//             </div>
//         )
//     }
// }


// export default ServiceHistory;
//
