import React from "react";
import { Link } from 'react-router-dom';


class ManufacturerList extends React.Component {
    constructor() {
        super();
        this.state = {
            manufacturers: []
        };
    }


    async componentDidMount() {
        const response = await fetch ("	http://localhost:8100/api/manufacturers/");
        if (response.ok) {
            const data = await response.json();
            this.setState({ manufacturers: data.manufacturers })
            console.log(this.state)
        }
        else {
            console.error(response)
        }
    }


    render () {
        return (
            <div className='container pt-5'>
                <div className="d-grid gap-2 d-sm-flex justify-content-sm-end mt-2">
                    <Link to="/manufacturers/new" className="btn btn-success btn-lg px-4 gap-3">Add Manufacturer</Link>
                </div>
                <h1>Manufacturers</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>

                        </tr>
                    </thead>
                    <tbody>
                        {this.state.manufacturers.map(m => {
                                return (
                                    <tr key={m.id }>
                                        <td>{ m.name }</td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </table>
            </div>
        )
    }
}


export default ManufacturerList;
