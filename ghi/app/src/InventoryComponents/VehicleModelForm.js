import React from "react";


class VehicleModelForm extends React.Component {
    constructor() {
        super();
        this.state = {
            name: "",
            picture_url: "",
            manufacturer_id: "",
            manufacturers: [],
        }
    }


    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state};
        delete data.manufacturers;

        const modelsURL = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
             },
        };
        const response = await fetch(modelsURL, fetchConfig);

        if (response.ok) {
            const newVehicleModel = await response.json();

            const cleared = {
                name: "",
                picture_url: "",
                manufacturer_id: "",
            };
            this.setState(cleared);
            const successAlert = document.getElementById("success-message")
            successAlert.classList.remove("d-none")
        }
        else {
            const successAlert = document.getElementById("error-message")
            successAlert.classList.remove("d-none")
        }
    }


    handleInputChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({...this.state, [name]: value})
    }


    async componentDidMount() {
        const manufacturersUrl = "	http://localhost:8100/api/manufacturers/";
        const response = await fetch(manufacturersUrl);

        if (response.ok) {
            const data = await response.json();
            this.setState({manufacturers: data.manufacturers})
        }
    }


    render() {
        return (
            <div className='container pt-5'>
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add Vehicle Model</h1>


                        <form onSubmit={this.handleSubmit} id="create-vehicle-model-form">

                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>


                        <div className="form-floating mb-3">
                            <input onChange={this.handleInputChange} value={this.state.picture_url} placeholder="Picture<" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                            <label htmlFor="picture_url">Picture</label>
                        </div>


                        <div className="mb-3">
                            <select onChange={this.handleInputChange} value={this.state.manufacturer_id} required id="manufacturer_id" name="manufacturer_id" className="form-select">
                            <option value="">Choose a Manufacturer</option>

                            {this.state.manufacturers.map(manufacturer_id => {
                                return (
                                    <option key={manufacturer_id.id} value={manufacturer_id.id}>
                                        {manufacturer_id.name}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-success">Add</button>
                        </form>

                        <div className="alert alert-success d-none mt-5" id="success-message">
                            Successfully added a new vehicle model!
                        </div>

                        <div className="alert alert-danger d-none mt-5" id="error-message">
                            URL address too long, try another one!
                        </div>


                    </div>
                </div>
            </div>
        )
    }
}


export default VehicleModelForm;
