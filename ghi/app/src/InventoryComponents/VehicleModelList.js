import React from "react";
import { Link } from 'react-router-dom';

class VehicleModelList extends React.Component {
    constructor() {
        super();
        this.state = {
            models: []
        };
    }


    async componentDidMount() {
        const response = await fetch ("http://localhost:8100/api/models/");
        if (response.ok) {
            const data = await response.json();
            this.setState({ models: data.models })
        }
        else {
            console.error(response)
        }
    }


    render () {
        return (
            <div className='container pt-5'>
                <div className="d-grid gap-2 d-sm-flex justify-content-sm-end mt-2">
                    <Link to="/vehicle-models/new" className="btn btn-success btn-lg px-4 gap-3">Add New Vehicle Model</Link>
                </div>
                <h1>Vehicle Models</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.models.map(model => {
                                return (
                                    <tr key={ model.id }>
                                        <td>{ model.name }</td>
                                        <td>{ model.manufacturer.name }</td>
                                        <td>
                                            <img src={ model.picture_url } alt="vehicle-model-picture" width="250px"/>
                                        </td>
                                    </tr>
                                );
                            })}
                    </tbody>
                </table>
            </div>
        )
    }
}


export default VehicleModelList;
