from django.db import models

# reverse
from django.urls import reverse



# Create your models here.


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17)

    def __str__(self):
        return self.vin




class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveIntegerField(null=True, unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_list_technicians", kwargs={"pk": self.id})




class  Appointment(models.Model):
    vin = models.CharField(max_length=17)
    customer_name = models.CharField(max_length=200)
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    reason = models.TextField()
    vip = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)

    technician = models.ForeignKey(
        "Technician",
        related_name="appointments",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.customer_name
    
    def get_api_url(self):
        return reverse("api_list_appointments", kwargs={"pk": self.id})

    



    


    
