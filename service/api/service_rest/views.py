from django.shortcuts import render
from service_rest.models import Technician, Appointment, AutomobileVO
from django.http import JsonResponse
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods

# Create your views here.


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [ 
        "import_href",
        "vin",
    ]



class TechniciansListEncoder(ModelEncoder):
    model = Technician
    properties = [ 
        "name",
        "id",
    ]




class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [ 
        "customer_name",
        "vin",
        "vip",
        "date",
        "time",
        "reason",
        "technician",
        "finished",
        "id",
    ]

    encoders = {
        "technician": TechniciansListEncoder()
    }




class AppointmentsListEncoder(ModelEncoder):
    model = Appointment
    properties = [ 
        "customer_name",
        "vin",
        "vip",
        "date",
        "time",
        "reason",
        "technician",
        "finished",
        "id",
    ]

    encoders = {
        "technician": TechniciansListEncoder()
    }




@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all().order_by("date")

        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentsListEncoder,
        )

    else:
        content = json.loads(request.body)

        technician = Technician.objects.get(id=content["technician"])
        content["technician"] = technician

        try:
            vin = AutomobileVO.objects.get(vin=content["vin"])
            content["vip"] = True
        except AutomobileVO.DoesNotExist:
            content["vip"] = False
        
        appointment = Appointment.objects.create(**content)

        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )




@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointment(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )

    
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )

    
    else:
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician = Technician.objects.get(id=content["technician"])
                content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                    {"message": "Invalid technician"},
                    status=400,
                )
        Appointment.objects.filter(id=pk).update(**content)

        appointment = Appointment.objects.get(id=pk)

        return JsonResponse(
            appointment, encoder=AppointmentDetailEncoder, safe=False
        )




@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()

        return JsonResponse(
            {"technicians": technicians},
            encoder=TechniciansListEncoder,
        )

    else:
        content = json.loads(request.body)

        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechniciansListEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Invalid technician"},
            )
